package com.pym.salaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Position {
    CEO("대표"),
    DIRECTOR("이사"),
    DEPUTY("차장"),
    MANAGER("과장"),
    ASSISTANT("대리"),
    STAFF("사원");

    private final String name;
}
