package com.pym.salaryapi.model;

import com.pym.salaryapi.enums.Position;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryBaseInfoChangeRequest {
    private String employeeId;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private Position position;
    private Double preTax;
}
