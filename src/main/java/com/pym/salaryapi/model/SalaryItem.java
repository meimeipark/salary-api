package com.pym.salaryapi.model;

import com.pym.salaryapi.enums.Position;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryItem {
    private Long id;
    private String employeeId;
    private String name;
    private String positionName;
    private Double preTax;
    private Double taxFree;
}
