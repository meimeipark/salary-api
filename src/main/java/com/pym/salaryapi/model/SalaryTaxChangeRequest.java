package com.pym.salaryapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryTaxChangeRequest {
    private Double nationalTax;
    private Double healthTax;
    private Double employmentTax;
    private Double careTax;
    private Double incomeTax;
    private Double taxFree;
}
