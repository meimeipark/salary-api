package com.pym.salaryapi.model;

import com.pym.salaryapi.enums.Position;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalaryResponse {
    private Long id;
    private String employeeId;
    private String name;
    private String position;
    private Double preTax;
    private Double nationalTax;
    private Double healthTax;
    private Double employmentTax;
    private Double careTax;
    private Double incomeTax;
    private Double taxFree;
    private Double afterTax;
}
