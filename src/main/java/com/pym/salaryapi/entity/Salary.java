package com.pym.salaryapi.entity;

import com.pym.salaryapi.enums.Position;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String employeeId;

    @Column(nullable = false, length = 30)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 30)
    private Position position;

    @Column(nullable = false)
    private Double preTax;

    @Column(nullable = false)
    private Double nationalTax;

    @Column(nullable = false)
    private Double healthTax;

    @Column(nullable = false)
    private Double employmentTax;

    @Column(nullable = false)
    private Double careTax;

    @Column(nullable = false)
    private Double incomeTax;

    @Column(nullable = false)
    private Double taxFree;

}
