package com.pym.salaryapi.service;

import com.pym.salaryapi.entity.Salary;
import com.pym.salaryapi.model.*;
import com.pym.salaryapi.repository.SalaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SalaryService {
    private final SalaryRepository salaryRepository;


    public void setSalary (SalaryRequest request){
        Salary addData = new Salary();
        addData.setEmployeeId(request.getEmployeeId());
        addData.setName(request.getName());
        addData.setPosition(request.getPosition());
        addData.setPreTax(request.getPreTax());
        addData.setNationalTax(request.getNationalTax());
        addData.setHealthTax(request.getHealthTax());
        addData.setEmploymentTax(request.getEmploymentTax());
        addData.setCareTax(request.getCareTax());
        addData.setIncomeTax(request.getIncomeTax());
        addData.setTaxFree(request.getTaxFree());

        salaryRepository.save(addData);
    }


    public List<SalaryItem> getSalaryList(){
        List<Salary> originList = salaryRepository.findAll();

        List<SalaryItem> result = new LinkedList<>();

        for(Salary salary: originList){
            SalaryItem addItem = new SalaryItem();
            addItem.setId(salary.getId());
            addItem.setEmployeeId(salary.getEmployeeId());
            addItem.setName(salary.getName());
            addItem.setPositionName(salary.getPosition().getName());
            addItem.setPreTax(salary.getPreTax());
            addItem.setTaxFree(salary.getTaxFree());

            result.add(addItem);
        }
        return result;
    }

    public SalaryResponse getSalary(long id){
        Salary originData = salaryRepository.findById(id).orElseThrow();

        SalaryResponse response = new SalaryResponse();
        response.setId(originData.getId());
        response.setEmployeeId(originData.getEmployeeId());
        response.setName(originData.getName());
        response.setPosition(originData.getPosition().getName());
        response.setPreTax(originData.getPreTax());
        response.setNationalTax(originData.getNationalTax());
        response.setHealthTax(originData.getHealthTax());
        response.setEmploymentTax(originData.getEmploymentTax());
        response.setCareTax(originData.getCareTax());
        response.setIncomeTax(originData.getIncomeTax());
        response.setTaxFree(originData.getTaxFree());
        response.setAfterTax(originData.getPreTax()-originData.getNationalTax()-originData.getHealthTax()-originData.getEmploymentTax()-originData.getCareTax()-originData.getIncomeTax());

        return response;
    }

    public void putSalaryTax(long id, SalaryTaxChangeRequest request) {
        Salary originData = salaryRepository.findById(id).orElseThrow();
        originData.setNationalTax(request.getNationalTax());
        originData.setHealthTax(request.getHealthTax());
        originData.setEmploymentTax(request.getEmploymentTax());
        originData.setCareTax(request.getCareTax());
        originData.setIncomeTax(request.getIncomeTax());
        originData.setTaxFree(request.getTaxFree());

        salaryRepository.save(originData);
    }

    public void putBaseInfo(long id, SalaryBaseInfoChangeRequest request){
        Salary originData = salaryRepository.findById(id).orElseThrow();
        originData.setEmployeeId(request.getEmployeeId());
        originData.setName(request.getName());
        originData.setPosition(request.getPosition());
        originData.setPreTax(request.getPreTax());

        salaryRepository.save(originData);
    }


    public void delSalary(long id){salaryRepository.deleteById(id);}


}

