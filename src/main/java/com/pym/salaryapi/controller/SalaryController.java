package com.pym.salaryapi.controller;


import com.pym.salaryapi.model.*;
import com.pym.salaryapi.service.SalaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/salary")
public class SalaryController {
    private final SalaryService salaryService;

    @PostMapping("/people")
    public String setSalary (@RequestBody SalaryRequest request){
        salaryService.setSalary(request);

        return "추가";
    }

    @GetMapping("/all")
    public List<SalaryItem> getSalaryList(){
        return salaryService.getSalaryList();
    }

    @GetMapping("/detail/{id}")
    public SalaryResponse getSalary(@PathVariable long id){
        return salaryService.getSalary(id);
    }

    @PutMapping("/tax/{id}")
    public String putSalaryTax(@PathVariable long id, @RequestBody SalaryTaxChangeRequest request){
        salaryService.putSalaryTax(id, request);

        return "세금수정";
    }

    @PutMapping("/info/{id}")
    public String putBaseInfo(@PathVariable long id, @RequestBody SalaryBaseInfoChangeRequest request){
        salaryService.putBaseInfo(id, request);

        return "정보수정";
    }

    @DeleteMapping("/{id}")
    public String delSalary(@PathVariable long id){
        salaryService.delSalary(id);

        return "삭제";
    }
}
