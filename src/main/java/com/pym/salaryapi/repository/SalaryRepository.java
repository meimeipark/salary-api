package com.pym.salaryapi.repository;

import com.pym.salaryapi.entity.Salary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalaryRepository extends JpaRepository <Salary, Long> {
}
